
/*!
 * Copyright (c) 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Eric Poirier <eric.poirier@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import $, { type } from 'jquery';
import Mustache from 'mustache';
import 'jquery-match-height';
import getProjectsData from "eclipsefdn-solstice-assets/js/api/eclipsefdn.projects"

const EclipseFdnProjectsHome = (async () => {

  const element = document.querySelector('.featured-projects-home');
  if (!element) {
    return;
  }

  const options = {
    ...$(element).data(),
  };

  let projectsData = await getProjectsData(options.url, []);

  let resultsProjects = [];
  Promise.allSettled([projectsData])
    .then(function (responses) {
      if (responses[0].status === 'fulfilled') {
        const projects = responses[0].value;
        if (projects.length > 0) {
          if (options.projectIds.length > 0) {
            const projectIds = options.projectIds.split(",");

            $(projects).each(function (key, project) {
              if ($.inArray(project.project_id, projectIds) != -1) {
                resultsProjects.push(project);
              }
            });
          }
          else {
            resultsProjects = projects;
          }
        }
      }

      const projectLeads = options.projectLeads.split(",");

      $(resultsProjects).each(function (key, project) {
        $(project.project_leads).each(function (project_lead_key, project_lead) {
          if ($.inArray(project_lead.username, projectLeads) != -1) {
            resultsProjects[key].project_lead_full_name = project_lead.full_name;
            resultsProjects[key].project_lead_username = project_lead.username;
          }
        });
      });

      const data = {
        items: resultsProjects,
      };

      // Render component
      let html = '';
      if (options.templateId !== '' && document.getElementById(options.templateId)) {
        const theme = document.getElementById(options.templateId).innerHTML;
        html = Mustache.render(theme, data);
      } else {
        html = template(data);
      }

      element.innerHTML += html;

      if (resultsCategories.length <= 0) {
        resultsCategories = tags;
      }

      $.fn.matchHeight._applyDataApi();
    })
    .catch(err => console.log(err));
})();

export default EclipseFdnProjectsHome;
