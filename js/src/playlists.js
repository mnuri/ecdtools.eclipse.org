/*!
 * Copyright (c) 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

const apiBaseUrl = 'https://youtube.googleapis.com/youtube/v3'

const getPlaylistFromAPI = async (token, playlistId) => {
    try {
        const response = await fetch(`${apiBaseUrl}/playlistItems?key=${token}&playlistId=${playlistId}&part=snippet`);
        const responseJson = await response.json();

        return { playlist: responseJson }
    } catch (error) {
        return { error }
    }
}

const getPlaylists = async (token, playlistIds) => {
    // Retrieve playlists from API
    let playlists = await Promise.all(playlistIds
        .map(async (id) => {
            const { playlist, error } = await getPlaylistFromAPI(token, id);

            if (error) return;

            return playlist;
        }))

    return playlists.filter(p => typeof p !== 'undefined');
}

const getLatestVideoFromPlaylist = (playlists) => {
    let latestVideos = playlists
        .map(playlist => playlist.items)
        .flat()
        .sort((v1, v2) => v1.snippet.publishedAt < v2.snippet.publishedAt ? 1 : -1);

    const latestVideo = latestVideos[0];

    return {
        date: new Date(latestVideo.snippet.publishedAt),
        title: latestVideo.snippet.title,
        description: latestVideo.snippet.description,
        videoId: latestVideo.snippet.resourceId.videoId
    };
}

const addLatestVideoToWhatsNew = async () => {
    const elem = document.querySelector('.whats-new-playlist');
    if (!elem) return;

    const token = elem['youtubeAPIToken'];
    if (!token) return;

    // Load static playlist data from data attribute
    const playlistBuffer = Buffer.from(elem.dataset.playlists, 'base64');
    const staticPlaylistsData = JSON.parse(playlistBuffer.toString());
    const playlistIds = staticPlaylistsData.map(p => p.playlist_id);

    // Obtain the latest video from all playlists
    const playlists = await getPlaylists(token, playlistIds);
    const latestVideo = getLatestVideoFromPlaylist(playlists);

    const titleElem = document.querySelector('.whats-new-title');
    titleElem.innerHTML = latestVideo.title;
    titleElem.href = `https://www.youtube.com/watch?v=${latestVideo.videoId}`

    const descriptionElem = document.querySelector('.whats-new-description');
    descriptionElem.innerHTML = latestVideo.description.substring(0, 200) + '...';

    const dateOptions = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hourCycle: 'h24',
    };

    const dateElem = document.querySelector('.whats-new-date');

    let formattedDate = latestVideo.date
        .toLocaleDateString(undefined, dateOptions)
        .toString()
        .replace('at', '-')

    dateElem.innerHTML = formattedDate;

}

export default addLatestVideoToWhatsNew;