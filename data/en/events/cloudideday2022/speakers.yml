items:
  - name: "Irina Artemeva"
    title: "TypeFox"
    img: "irina-artemeva.jpg"
    bio: |
      <p>Irina Artemeva is passionate about everything related to programming languages theory. This interest got started with Haskell, which showed her the multidimensional nature of programming paradigms, and blossomed into love for all programming languages in general. She has earned a master’s degree in Computer Science, and now works at TypeFox where she helps customers build their own programming languages and development environments. She deeply cares about improving the user experience of other software developers and is therefore happy to work on the language engineering framework Langium.</p>
  - name: "Arnaud Fiorini"
    title: "Polytechnique Montréal"
    img: "arnaud-fiorini.jpg"
    bio: |
      <p>Arnaud Fiorini is a research associate who helps students with their research on many topics around tracing. Arnaud is developing technical solutions for tracing and profiling issues in the HPC domain, mainly on GPU programming. He also contributes to open source projects at the Eclipse Foundation like Trace Compass and Theia.</p>
  - name: "Jonas Helming"
    title: "EclipseSource"
    img: "jonas-helming.jpg"
    bio: |
      <p>Dr. Jonas Helming is CEO of EclipseSource as well as consultant, trainer and software engineer for building web-based and desktop-based tools. He is project lead and committer to various open source projects including Eclipse Theia. Jonas regularly published articles about tool and IDE related topics and is a regular conference speaker on these topics.</p>
  - name: "Laurent Balmelli"
    title: "Strong Network"
    img: "laurent-balmelli.jpg"
    bio: |
      <p>Laurent Balmelli is the co-founder, along with Ozrenko Dragic, of the company Strong Network, a Swiss security company providing a platform to deploy secure Cloud IDEs over a Zero-Trust Architecture. The company is backed by top European VCs OpenOcean and Wingman Ventures and has three office locations: Switzerland, Serbia, and the US. Before that, Laurent sold his last cybersecurity start-up, Strong Codes, to the US company Snapchat in 2016. He also worked 12 years at IBM, in particular at the Research Division in New York and Tokyo. He is active on the Medium publishing platform where he writes about security, digital life, and privacy.</p>
  - name: "Mélanie Bats"
    title: "Obeo"
    img: "melanie-bats.jpg"
    bio: |
      <p>Mélanie Bats is CTO at Obeo. In her daily work, she is mainly focused on managing the R&D team, which is focused on the development of modeling tools with Sirius. Mélanie is also a committer for the EEF and the Sirius projects, and involved in the Eclipse community as part of the Eclipse Planning Council. She is a free software activist who has organized and participated in free software events in the Toulouse area.</p>
  - name: "Michel Dagenais"
    title: "Polytechnique Montréal"
    img: "michel-dagenais.jpg"
    bio: |
      <p>Michel Dagenais is professor at Polytechnique Montreal in the department of Computer and Software Engineering. He authored or co-authored over one hundred scientific publications, as well as numerous free documents and free software packages in the fields of operating systems, distributed systems and multicore systems, in particular in the area of tracing and monitoring Linux systems for performance analysis. Most of his research projects are in collaboration with industry and generate free software tools among the outcomes. The Linux Trace Toolkit next generation, developed under his supervision, is now used throughout the world and is part of several specialized and general purpose Linux distributions.</p>
  - name: "Stefan Dirix"
    title: "EclipseSource"
    img: "stefan-dirix.jpg"
    bio: |
      <p>Stefan Dirix is a software architect and consultant at EclipseSource. He is the project lead of CDT.cloud Blueprint and contributes to several software projects inside and outside the Eclipse Cloud ecosystems. Stefan works on the design and development of modern web-based applications and IDEs based on VS Code and Eclipse Theia with a focus on embedded C/C++ development.</p>
  - name: "Peter Haumer"
    title: "IBM"
    img: "peter-haumer.jpg"
    bio: |
      <p>Dr. Peter Haumer works as a development lead and senior technical staff member for the IBM Wazi product platform, which offers tools and solution packages for z/OS Cloud-native DevOps. He serves as IBM's representative in the Eclipse Cloud DevTools community with a special interest in the projects Eclipse Che, Theia, and Open-VSX.</p>
  - name: "Philip Langer"
    title: "EclipseSource"
    img: "philip-langer.jpg"
    bio: |
      <p>Philip Langer is a principal software architect and general manager of EclipseSource in Austria. For many years now, Philip has been passionate about the modeling technologies of the Eclipse ecosystem. More recently web-/cloud-based tool development has become one of his main focus areas. In his daily work, Philip supports organizations in building web-based domain-specific modeling and engineering tools, including graphical modeling editors, model diff/merge with Git, and much more -- all based on open-source Eclipse technologies. Philip is the project lead of the Eclipse Graphical Language Server Platform (GLSP), as well as of EMF.cloud and CDT.cloud, and a committer on Sprotty, EMF Compare, and Papyrus.</p>
  - name: "Yordan Pavlov"
    title: "SAP"
    img: "yordan-pavlov.jpg"
    bio: |
      <p>Yordan Pavlov is project lead of Eclipse Dirigible and solution architect for the project XSK, both open source projects. He is also one of the major contributors on the two GitHub repositories. Yordan joined SAP in 2013 and was part of the core teams working on the SAP Cloud Platform, now known as SAP BTP. Recently he switched his focus mostly toward helping SAP customers and partners to migrate and develop their solutions on top of the SAP BTP.</p>
  - name: "Ilya Buziuk"
    title: "Red Hat"
    img: "ilya-buziuk.jpg"
    bio: |
      <p>Ilya Buziuk and David Kwon are software engineers at Red Hat. They work on container-based developer tools to accelerate software development and manage Eclipse Che hosted by Red Hat - https://workspaces.openshift.com/.</p>
  - name: "David Kwon"
    title: "Red Hat"
    img: "david-kwon.jpg"
    bio: |
      <p>Ilya Buziuk and David Kwon are software engineers at Red Hat. They work on container-based developer tools to accelerate software development and manage Eclipse Che hosted by Red Hat - https://workspaces.openshift.com/.</p>
  - name: "Matthew Khouzam"
    title: "Ericsson"
    img: "matthew-khouzam.jpg"
    bio: |
      <p>Matthew Khouzam is the product owner at Ericsson for OpenVSX, Theia and Trace Compass. He is a committer in the Trace Compass project and has several thousand reviews under his belt. In his job, he coordinates with academia, FOSS communities, customers and dev teams while trying to squeeze a patch out here and there. He is a fan of all things tech, cooking and dad jokes. Most importantly, he is a father of two, and a husband.</p>
  - name: "Vincent Fugnitto"
    title: "Ericsson"
    img: "vincent-fugnitto.jpg"
    bio: |
      <p>Vincent Fugnitto is a software developer for Ericsson Canada, who has been working on the Eclipse Theia framework since 2018. His main areas of focus in the project are general useability improvements, extensibility, user experience, and fostering a welcoming and helpful community.</p>
  - name: "Jesse Williams"
    title: "Amazon Web Services"
    img: "jesse-williams.jpg"
    bio: |
      <p>Jesse Williams has +10 years of experience leading product marketing for developer focused solutions and services. Jesse currently leads Product Marketing for the AWS Serverless Application Integration services. Prior to AWS, Jesse was the Head of Lifecycle Marketing for Red Hat Developer.  He got his start in developer marketing while leading the marketing efforts for Codenvy and open source Eclipse Che. Outside of work, he is involved with the Eclipse Cloud Development Tools Working Group, where he hosts Cloud Chats, a Twitch series focused on open source leadership and strategy, and serves as a board advisor for Stanza, an open source log agent that's part of the CNCF's OpenTelemetry project.</p>
  - name: "Mario Loriedo"
    title: "Red Hat"
    img: "mario-loriedo.jpg"
    bio: |
      <p>Mario Loriedo is a Senior Principal Software Engineer at Red Hat. He works on container-based tools to accelerate software development. He leads the Eclipse Che project.</p>
