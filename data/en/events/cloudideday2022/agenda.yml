dynamicTime: true
timezone: GMT-04
types:
    - name: Intro
      id: intro 
      color: "#accc66"
    - name: Session
      id: session 
      color: "#3a85b6"
    - name: Panel
      id: panel 
      color: "#fdb816"
sets:
  - name: Wednesday, April 27, GMT-04
    items:
    - name: Welcome & Introduction
      presenters: 
        - John Kellerman
      time: 10:00 - 10:15
      type: intro
    - name: Diagram Editors in Cloud IDEs
      presenters: 
        - Philip Langer
      time: 10:25 - 10:55
      type: session
      abstract: |
            <p>The support for textual programming languages is excellent in modern, web-based IDEs, such as Eclipse Theia and VS Code. The hugely popular language server protocol (LSP) and its integration with the Monaco code editor enabled lifting great code editing experience to the web. However, what about diagram editors and graphical languages? Such diagram editors play a key role in domain-specific tools, modeling suites, and low-code platforms. How can a modern diagramming experience be achieved in cloud-based tools?</p>
            <p>GLSP transfers the successful architecture pattern of LSP to graphical languages, including a diagram editor client (“Monaco for diagrams'') and the ability to implement the diagram logic as a server component, which can be hooked up to any data source. While GLSP provides generic support for many diagram aspects, such as shapes, routing, a tool palette, animations, etc. out of the box, the framework is built to be extensible and open for customization to allow for developing truly domain-specific editors with the full flexibility of the underlying modern web-technologies, such as TypeScript, HTML5, CSS3 and SVG. For a seamless integration into a tool platform, GLSP provides ready-to-be-used glue components for Eclipse Theia and VS Code. Eclipse GLSP is one of the most active projects in the Eclipse Cloud development tools ecosystem and is increasingly adopted by key players of various domains lifting their rich diagram editors to the web.</p>
            <p>We will give a demonstration of the capabilities of Eclipse GLSP and provide an overview of its architecture, especially from a viewpoint of a diagram developer. Moreover, we will highlight the conceptual similarities and differences between LSP and GLSP and why they are crucial for a successful adoption of the LSP approach for diagrams. Based on that, we would like to dive into an open Q&A and interactive discussion with the audience.</p>
    - name: Using Langium to Create DSLs in the Cloud
      presenters: 
        - Irina Artemeva
      time: 11:05 - 11:35
      type: session
      abstract: |
            <p>Developing a domain specific language (DSL) is a lengthy process, requiring creativity, domain knowledge and technical affinity. While commonly used language engineering frameworks have proven to be effective for building DSLs, they are either tightly coupled to traditional desktop IDEs or require a non-web tech stack. This talk presents a lightweight solution to both of these problems by using the Langium framework. This enables building Theia-compatible plugins with the VS Code extension format so they can be easily deployed in a cloud IDE, and distributed with Open VSX.</p>
    - name: Building Custom C/C++ Tools in the Web
      presenters: 
        - Stefan Dirix
      time: 11:45 - 12:15
      type: session
      abstract: |
            <p>Do you want to develop a customized tool for C/C++ development in the web/cloud? In this talk, we will provide a state-of-the-art overview and recommendations about available tools, extensions and frameworks for building web-based C/C++ tooling. We'll present CDT.cloud, a new project hosted at the Eclipse Foundation that provides a home for several C/C++ tool technologies for web- and cloud-based tools. Furthermore, we introduce CDT.cloud Blueprint, a template tool for C/C++ development based on open source components.</p>
            <p>Many domain-specific C/C++ tool chains are currently in the process of migrating to web-based tech stack and a cloud-based infrastructure. This migration involves a lot of critical technology choices and important integration work to reach an industry-ready quality of integrated language and development support on a modern UI and flexible tool architecture. These decisions include choosing a language server, selecting C/C++ extensions, defining build configuration management and choosing solutions for a debug adapter, memory view, tracing, register view and many others.</p>
            <p>In this talk, we will discuss the most common bits and pieces of a C/C++ tool chain in general and in addition put a special focus on embedded development. We will discuss the technology selection, provide recommendations and also point out components currently under development to look out for in the near future. We will also demonstrate how to integrate all that into your custom C/C++ tool based on an open-source example tool called CDT.cloud Blueprint that may act as the basis for your custom C/C++ tool product. Besides looking at this specific project we'll also give a brief overview over CDT.cloud in general, a new project hosted at the Eclipse Foundation that provides a home for several C/C++ tool technologies for web- and cloud-based tools.</p>
            <p>For this overview, we compiled the experiences gained from various customer projects, in which we are migrating to web-based tooling as well as an additional survey among partner companies. Therefore this talk will provide you with a profound overview of current trends and guidelines to get started with your own web-based tooling project for C/C++ development.</p>
    - name: 25-Minute Break
      presenters: 
        - 
      time: 12:15 - 12:40
      type: break
    - name: Bringing Eclipse Dirigible to the Next Level at SAP with Project XSK
      presenters: 
        - Yordan Pavlov
      time: 12:40 - 13:10
      type: session
      abstract: |
            <p>In this session Yordan Pavlov will share how Eclipse Dirigible was adopted inside SAP as a technology stack and foundation layer for the open-source project XSK. He'll show different integration options, and which one was selected and why for project XSK. He'll briefly cover what project XSK is about and why Eclipse Dirigible was the natural choice for it. He'll explain how SAP HANA XS Classic applications can be migrated to project XSK and how Eclipse Dirigible is facilitating this process.</p>
    - name: Mainframe Development in the Cloud
      presenters: 
        - Peter Haumer
      time: 13:20 - 13:50
      type: session
      abstract: |
            <p>Mainframes run the world. 85% of business transactions are processed on a mainframe and many people often do not realize that every time they use a credit card, submit an insurance claim, or book a flight they indirectly interact with an IBM z mainframe. Many distributed developers often do not realize that z can be part of today’s Cloud-native offerings and development infrastructure. A z/OS system can be just another (although massively powerful) node in your network, participating in modern continuous DevOps tool chains and workflows. Developers can use all the same modern development tools (including VS Code or Eclipse Che) to write, build, test, and deliver their enterprise applications. As open source plays a key role in all of software development so does it for z/OS development. In this presentation we will walk you through end-to-end developer workflows performed entirely in the Cloud for maintaining a COBOL application utilizing Eclipse and other open-source tools, as well as IBM capabilities layered on top of open source. From provisioning and configuring a z/OS development and test system in the Cloud with Ansible and Zowe, cloning your code via Git into a zero-install, fully customizable Eclipse Che-based development environment, supporting developers with language servers, debug clients, and user build tools, to running a builds/tests/deploy pipeline with an open-source SCM runner on z/OS. We will also show you how vendors such as IBM and Red Hat add value to Eclipse Cloud DevTools and other projects through our Kubernetes certification program that applies stringent sets of over 300 quality criteria against all images we ship with our solution packages that result into fixes and code donations back to these projects, helping to improve them over time to reach many of the same criteria.</p>
    - name: "Moving Tools to the Cloud: Challenges and Best Practices"
      presenters: 
        - Jesse Williams (moderator)
        - Vincent Fugnitto
        - Philp Langer
        - Mario Loriedo
      time: 14:00 - 14:45
      type: panel
      abstract: |
            <p>Come to this panel discussion to hear our experts with years of development experience talk about their experiences building tools for and moving tools to the cloud; best practices, things they tripped over that they'd do differently, and customer adoption experiences.</p>

  - name: Thursday, April 28, GMT-04
    items:
    - name: Introduction
      presenters: 
        - John Kellerman
      time: 10:00 - 10:05
      type: intro
    - name: Create Graphical Studios with Sirius Web
      presenters: 
        - Mélanie Bats
      time: 10:15 - 10:45
      type: session
      abstract: |
            <p>This talk is for you if you want to build web-based graphical modeling tools.</p>
            <p>Eclipse Sirius is an open source project to create domain-specific modeling workbenches. Sirius allows you to represent your data, map them to graphical representations and take advantage of your data's semantic!</p>
            <p>Using Sirius Web you can create dedicated tools to</p>
            <ol>
            <li>Represent and edit your data graphically</li>
            <li>Represent and edit your data graphically</li>
            </ol>
            <p>Participate to see how to</p>
            <ul>
            <li>Define your domain</li>
            <li>Map your data to visual representation</li>
            <li>Take advantage of your semantics</li>
            <li>See your studio alive!</li>
            </ul>
    - name: Getting Started with Eclipse Theia
      presenters: 
        - Jonas Helming
      time: 10:55 - 11:25
      type: session
      abstract: |
            <p>Are you interested in building a custom IDE or tool for your domain that runs in the cloud but also as a desktop application? In this talk, we will introduce you to Eclipse Theia, the next generation open source platform for building custom tools and IDEs!</p>
            <p>Eclipse Theia currently builds a lot of momentum and is being adopted in various domains as the basis for modern and web-based toolchains. However, getting started with a powerful technology like Theia and the extensive ecosystem around it is sometimes overwhelming. Especially newcomers are often confronted with a recurring set of frequently asked questions.</p>
            <p>In this talk, we provide an accessible but still comprehensive introduction to the world of Eclipse Theia. We start with the motivation of the project, describe its core features and provide an overview of the ecosystem and its current adopters. Furthermore, we focus on frequently asked questions including:</p>
            <ul>
            <li>What are similarities and differences to VS Code</li>
            <li>How to use/consume/adopt Eclipse Theia</li>
            <li>How to communicate with the community</li>
            <li>How to contribute to Eclipse Theia</li>
            <li>Etc.</li>
            </ul>
            <p>Our goal is to provide you with a jump start to the vibrant and active ecosystem around Eclipse Theia as the next generation platform for building web- and cloud-based tools.</p>
    - name: Protect Your Cloud-IDE Development Process Against Data Leaks
      presenters: 
        - Laurent Balmelli
      time: 11:35 - 12:05
      type: session
      abstract: |
            <p>We explain how Cloud IDEs are the perfect means to deliver a zero-trust architecture across a code development process that involves a global team of coders, such that the organization's source code, data and credentials are protected from leaks without the need of any endpoint security.</p>
    - name: 20 Minute Break
      presenters: 
        - 
      time: 12:05 - 12:25
      type: break
    - name: An IDE to Develop, Trace and Debug Cloud and HPC Applications
      presenters: 
        - Arnaud Fiorini
        - Michel Dagenais
      time: 12:25 - 12:55
      type: session
      abstract: |
            <p>The DORSAL group at Polytechnique, in collaboration with Ericsson, Ciena, AMD and EfficiOS, is working on an IDE based on Eclipse Theia to develop, trace and debug large Cloud and HPC distributed applications. Theia and Trace Compass already provide most of the needed functionality, including support for HPC languages such as OpenCL through the clangd language server. Our work thus focuses on Trace Compass scalability to handle large traces from hundreds or thousands of nodes, GPU tracing and profiling in Trace Compass by interfacing to ROCm, and combined CPU and GPU code debugging by interfacing to ROCgdb. The presentation will contain several small demos.</p>
    - name: Run Your Favorite IDE on Kubernetes with Eclipse Che
      presenters: 
        - Ilya Buziuk
        - David Kwon
      time: 13:05 - 13:35
      type: session
      abstract: |
            <p>In this session, you will learn about the cutting-edge version of Eclipse Che - a next-generation container development platform that allows you to run your favorite IDE on Kubernetes.</p>
            <p>Eclipse Che is now powered by the DevWorkspace Engine, which is a Kubernetes operator. It introduces a Custom Resource to define development environments and a controller that manages the resulting Kubernetes objects. That means that developers can specify their tooling, environment, and IDE as a Kubernetes object and share it with their team. It is even possible to start a workspace with a “kubectl apply” CLI command!</p>
            <p>In this talk, we are going to demo repeatable Che-Theia, VS Code, and IntelliJ-based workspaces, running on OpenShift, and managed by the DevWorkspace operator.</p>
            <p>We are also going to provide instructions for updating existing Che Server-based instances to the new DevWorkspace engine.</p>
    - name: Deploying OpenVSX in a Corporate-Friendly Way
      presenters: 
        - Matthew Khouzam
      time: 13:45 - 14:15
      type: session
      abstract: |
            <p>OpenVSX is great. It is the marketplace that allows Theia-based IDEs to install plug-ins and benefit from a community. This talk will explore what is missing that would allow larger companies as well as security-minded individuals to successfully deploy and use OpenVSX internally. It is also a call to action for coordinating our efforts to get there.</p>
    - name: Event Closing
      presenters: 
        - Tim deBoer
      time: 14:25 - 14:40
      type: intro
