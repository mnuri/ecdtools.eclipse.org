---
title: 'Cloud IDE Days 2022'
headline: 'Cloud IDE Days 2022'
custom_jumbotron: '<h2 class="margin-top-10">Guiding the Future of IDE Development</h2><p class="margin-top-20">Virtual Conference | April 27 - 28, 2022</p>'
date: 2022-02-02T08:00:00-24:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: 'header-ide-event-2022'
hide_breadcrumb: true
container: 'container-fluid idesummit-event'
summary: 'This is the 2nd annual virtual event for IDE developers, with an emphasis on tools and best practices for development in the cloud with the goal of guiding the future direction of IDE technology. The event is open to anyone interested in adopting or currently building Cloud IDEs. It’s hosted by the Eclipse Cloud DevTools Working Group, part of the Eclipse Foundation. This is a vendor-neutral group focused on industry collaboration on open source cloud development tools.'
layout: single
featured_section_class: 'featured-idesummit-event'
---

{{< grid/section-container id="committee" class="row white-row text-center">}}
  <h2>Thank You!</h2>
  <p class="margin-bottom-20">Thanks to everyone who helped make Cloud IDE Days 2022 such a success, especially our speakers, program committee, and&nbsp;member&nbsp;companies.</p>
  <p class="margin-bottom-20"><a href="https://www.youtube.com/watch?v=Ug-90u8Ra0E&list=PLy7t4z5SYNaRPFvy68DJA44C-kwhG-bUN">Watch the talk recordings</a> on the Eclipse Foundation YouTube channel.</p>
{{</ grid/section-container >}}

{{< grid/section-container containerClass="padding-top-40 padding-bottom-30" >}}
{{< grid/div class="cloud-ide-2022-registration container" isMarkdown="false" >}}
  <h2 class="text-center margin-bottom-30">Join the Conversation</h2>

  <div class="margin-bottom-30 row">
    <div class="col-sm-3 text-center">
      <h3 class="uppercase">What:</h3>
    </div>
    <div class="col-sm-21 margin-top-10">
      <p>
        Cloud IDE Days 2022 is our second annual virtual event for IDE developers, with an emphasis on tools and best practices for development in the cloud.
      </p>
    </div>
  </div>
  <div class="margin-bottom-30 row">
    <div class="col-sm-3 text-center">
      <h3 class="uppercase">Why:</h3>
    </div>
    <div class="col-sm-21 margin-top-10">
      <p>
        The objective of this event is to openly discuss and share ideas around Cloud IDEs and cloud development. What do developers need in the next generation of remote or web-based IDEs? What new IDE tools will best help them build cloud-native applications? How can existing IDEs more efficiently harness the cloud?
      </p>
    </div>
  </div>
  <div class="margin-bottom-30 row">
    <div class="col-sm-3 text-center">
      <h3 class="uppercase">Who:</h3>
    </div>
    <div class="col-sm-21 margin-top-10">
      <p>
        The event is open to anyone interested in adopting or currently building Cloud IDEs. Cloud IDE Days is hosted by the Eclipse Cloud DevTools Working Group, part of the Eclipse Foundation, a vendor-neutral group focused on industry collaboration on open source cloud development tools.
      </p>
    </div>
  </div>
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-lighter-bg agenda-ide-2022">}}
{{< grid/div class="padding-bottom-20" isMarkdown="false" >}}
{{< events/agenda year="events" event="cloudideday2022" >}}
{{</ grid/div >}}
{{</ grid/section-container >}}


{{< grid/section-container id="speakers" class="row text-center padding-top-40 padding-bottom-40">}}
{{< events/user_display event="cloudideday2022" year="events" title="Speakers" source="speakers" imageRoot="/events/cloudideday2022/images/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

{{< grid/section-container id="committee" class="row white-row text-center">}}
{{< grid/div class="text-center" isMarkdown="false" >}}
{{< events/user_display event="cloudideday2022" year="events" title="Meet the Program Committee" source="committee" imageRoot="/events/cloudideday2022/images/" displayLearnMore="false" />}}
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="organizers" class="featured-section-row text-center">}}
{{< grid/div isMarkdown="true" >}}
## Organized by
Cloud IDE Days is hosted by the Eclipse Cloud DevTools Working Group, part of the Eclipse Foundation, a vendor-neutral group focused on industry collaboration in open source cloud development tools.
{{</ grid/div >}}
{{< events/sponsors event="cloudideday2022" year="events" headerClass="hidden" source="organizers" title=" " useMax="false" displayBecomeSponsor="false" >}}
{{</ grid/section-container >}}

{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}
